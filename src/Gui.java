import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class Gui extends JFrame {
 
	JFrame frame ;
	JLabel name1,name2;
	JTextArea text1,Show ;
	JTextField text2 ;	
	JPanel    panel1,panel2,Panel3;
	JButton  button1,button2;
	String str1;
	
	public Gui(){
		createGui();
	}
	public void createGui(){
		
		name1 = new JLabel("WORD :");
		name2 = new JLabel("CUT :");
		text1	  = new JTextArea(2,20);
		text2     = new JTextField(10);
		Show   = new JTextArea(5,25);
		panel1    = new JPanel();
		panel2    = new JPanel();
		Panel3 =	new JPanel();
		button1 = new JButton("Enter");
		button2 = new  JButton("Exit");		
		panel1.add(name1);
		panel1.add(text1);		
		panel1.add(name2);
		panel1.add(text2);		
		panel2.add(Show);
		setLayout(new BorderLayout());
		add(panel1,BorderLayout.NORTH);
		add(panel2,BorderLayout.CENTER);
		Panel3.add(button1);
		Panel3.add(button2);
		add(Panel3,BorderLayout.EAST);
	}
	public void setListener(ActionListener list) {
		button1.addActionListener(list);
		button2.addActionListener(list);
	}
	
}
